#include <stdio.h>
#include <termios.h>
#include <python2.7/Python.h>

int main()
{
    //Initialize the python instance
    Py_Initialize();

    //Run a python file
    FILE *PScriptFile = fopen("ultrasonic.py", "r");
    if (PScriptFile)
    {
        PyRun_SimpleFile(PScriptFile, "ultrasonic.py");
        fclose(PScriptFile);
    }
    //Close the python instance
    Py_Finalize();
}
